#ifndef _CHARACTER_HG_
#define _CHARACTER_HG_

#include <vector>
#include "Observer.h"

class Character
{
public:
	Character();
	~Character();
	void RegisterObserver(Observer	* observer);
	void UnregisterObserver(Observer* observer);
	void NotifyObserver(string message);

	vector<Observer*> observerCollection;

};



#endif